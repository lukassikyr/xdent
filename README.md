# Global informations

## Requirements
- Node.js - (specific min. versions in package.json)
- NPM - (specific min. versions in package.json)
- Gulp - (specific min. versions in package.json)

```shell
sudo npm i gulp-cli -g
```


## Create App

```shell
npm install
```


## Run App

```shell
gulp watch OR npm watch
```



## Build App

```shell
gulp build
```


# Local Develop

## Git Hooks

### Pre commit

Musí projít npm script 'test', aby bylo možné commitnout, v případě nutnosti je možné v commmit message nastavit --no-verify


## IE 11 vývoj

Pro správné nastavení prefixů hlavně pro IE 11, provývoj je nutné odkomentovat v gulpfile.js autoprefixer v "sass:dev" tasku. Do buildu se autoprefixer přidává automaticky, ale pro urychlení vývoje/buildu v Chromu je autoprefixer záměrně vypnutý


## Gulp tasks

- **gulp build** - kompletní build projektu před GIT relasem, vytvoří a zkompiluje složku /js a /css minimalizovanou
- **gulp clean:dist** - odstraní složku /dist
- **gulp clean:mess** - odstraní nepodstatný zkompilovaný soubor /js/script.js , součást jiného tasku
- **gulp files:copy** - zkopíruje soubory do složky /dist
- **gulp fonts:copy** - zkopíruje fonty do složky /dist
- **gulp hbs:build** - zkompiluje všechny /src/templates/ a přidružené /templates/data/.json soubory
- **gulp hbs:default** - zkompiluje všechny .hbs soubory mimo podprojekty
- **gulp hbs:index** - zkompiluje všechny soubory ve složce /src, což by měl být jen index.hbs
- **gulp images** - zkompiluje a zmenší velikost obrázků a umístí je do složky /dist
- **gulp images:svg** - zkompiluje a zmenší velikost svg obrázků a ty pak umístí do složky /dist
- **gulp js:build** - zkompiluje /js soubory pro build
- **gulp js:copy** - zkopíruje složku js/libs do /dist, kde mají být umístěny typicky frameworky, které se nekompilují a jsou zde již zkompilované
- **gulp js:dev** - zkompiluje /js soubory pro vývoj
- **gulp lint** - kompletní lint všechny podflagů níže
- **gulp lint:html** - lint zbuildovaných HTML souborů ze složky /dist
- **gulp lint:js** - lint JS souborů ze složky /src
- **gulp lint:json** - lint JSON souborů ze složky /src
- **gulp lint:sass** - lint SCSS souborů přes SassLint ze složky /src
- **gulp lint:scss** - lint SCSS souborů přes StyleLint ze složky /src
- **gulp postcss** - pravidla pro POSTCSS, prozatím jen DoIUse ukázání použití CSS deklarací dle nadefinovaných prohlížečů
- **gulp root:copy** - zkopíruje XML a TXT soubory ze /src do /dist, typicky browserconfig.xml a nebo robots.txt
- **gulp sass:build** - zkompiluje SCSS soubory a minifikuje po dokončení vývoje nebo GIT release
- **gulp sass:dev** - zkompiluje SCSS soubory pro vývoj, neminifikuje a přidá maping
- **gulp sass:transform** - automatickou úpravu SCSS souborů dle daných pravidel dle stylefmt
- **gulp serve** - spouští BrowserSync, součást Watch tasku
- **gulp watch** - rozběhne localhost na portu 3000, tj. http://localhost:3000 ze složky /dist
