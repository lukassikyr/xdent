$(document).ready(function() {
	$('#datatable-xdent').DataTable({
		ajax: '../files/data.json',
		buttons: [
			{
				className: 'btn btn-secondary',
				exportOptions: {
					modifier: {
						page: 'current'
					}
				},
				extend: 'excel',
				text: 'Export current results to Excel'
			}
		],
		columns: [
			{
				data: 'code',
				searchable: false
			},
			{
				data: 'name'
			},
			{
				className: 'text-end',
				createdCell: function(td, cellData) {
					if(cellData > 1000) {
						$(td).addClass('table-danger');
					}
				},
				data: 'price',
				defaultContent: $.fn.dataTable.render.number,
				render: $.fn.dataTable.render.number('', '.', 0, '€ '),
				searchable: false
			},
			{
				className: 'text-end',
				orderable: false,
				render: function(data, type, row) {
					return '<button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modal-xdent">Detail of ' + row.code + '</button>';
				},
				searchable: false
			}
		],
		dom: '<"row"<"col"l><"col-8 text-end"B><"col-2"f>><"container"t>p',
		drawCallback: function() {
			var $sum = $('#datatable-xdent').DataTable().column(2, {page: 'current'}).data().sum();
			$('#total').html($sum);
		},
		lengthMenu: [ 5, 10, 20 ],
		pageLength: 5,
		responsive: true
	});

	// Modal
	var table = $('#datatable-xdent').DataTable();

	$('#datatable-xdent tbody').on('click', 'tr', function() {
		$('#datatable-code').text(table.row(this).data()['code']);
		$('#datatable-name').text(table.row(this).data()['name']);
		$('#datatable-price').text(table.row(this).data()['price']);
	});
});
