var gulp 			= require('gulp'),
	autoprefixer 	= require('autoprefixer'),
	browserSync 	= require('browser-sync'),
	clean 			= require('gulp-clean'),
	concat 			= require('gulp-concat'),
	cssnano 		= require('cssnano'),
	doiuse 			= require('doiuse'),
	eslint 			= require('gulp-eslint'),
	gulpSequence 	= require('gulp-sequence').use(gulp),
	gulpStylelint 	= require('gulp-stylelint'),
	gutil 			= require('gulp-util'),
	hb 				= require('gulp-hb'),
	headerfooter 	= require('gulp-headerfooter'),
	htmllint 		= require('gulp-htmllint'),
	imagemin 		= require('gulp-imagemin'),
	jsonlint 		= require('gulp-json-lint'),
	minify 			= require('gulp-minify'),
	postcss 		= require('gulp-postcss'),
	rename 			= require('gulp-rename'),
	sass 			= require('gulp-sass'),
	sassLint 		= require('gulp-sass-lint'),
	sourcemaps 		= require('gulp-sourcemaps'),
	stylefmt 		= require('gulp-stylefmt'),
	svgmin 			= require('gulp-svgmin'),

	browsers 		= ['last 2 versions', 'IE 10', 'IE 11'];

gulp.task('watch', function(cb) {
	gulpSequence('clean:dist', 'sass:transform', ['hbs:build', 'hbs:index', 'sass:dev', 'js:copy', 'js:dev', 'images', 'images:svg', 'fonts:copy', 'files:copy', 'root:copy'], 'lint', ['watchers', 'serve'])(cb);
});

gulp.task('watchers', (cb) => {
	gulp.watch('src/scss/**/*.scss', ['sass:dev', browserSync.reload]);
	gulp.watch('src/scss/_variables.scss', ['sass:dev', browserSync.reload]);
	gulp.watch('src/*.hbs', ['hbs:index', browserSync.reload]);
	gulp.watch([
		'src/templates/globalPartials/*.hbs',
		'src/templates/data/*.json'
	], ['hbs:build', browserSync.reload]);
	gulp.watch('src/templates/*.hbs', ['hbs:default', browserSync.reload]);
	gulp.watch('src/js/**/*.js', ['js:dev', browserSync.reload]);
	gulp.watch('src/fonts/**/*', ['fonts:copy', browserSync.reload]);
	gulp.watch('src/files/**/*', ['files:copy', browserSync.reload]);
	cb();
});

gulp.task('build', (cb) => {
	gulpSequence('clean:dist', 'sass:transform', ['hbs:build', 'hbs:index', 'sass:build', 'js:copy', 'js:build', 'images', 'images:svg', 'fonts:copy', 'files:copy', 'root:copy'], 'clean:mess', 'lint')(() => {
		console.log(gutil.colors.green('APP BUILT'));
		cb();
	});
});

gulp.task('clean:dist', () => {
	return gulp.src('dist', {read: false})
		.pipe(clean());
});

gulp.task('clean:mess', () => {
	return gulp.src('dist/js/script.js', {read: false})
		.pipe(clean());
});

gulp.task('serve', function(cb) {
	browserSync({
		browser: 'google chrome',
		notify: false,
		open: false,
		server: 'dist'
	})
	cb();
});

gulp.task('hbs:build', function(cb) {
	return gulp.src(['src/templates/*.hbs'])
		.pipe(hb({
			partials: 'src/templates/globalPartials/**/*.hbs',
			data: 'src/templates/data/**/*.json'
		}))
		.on('error', cb)
		.pipe(rename({
			extname: '.html'
		}))
		.pipe(headerfooter.footer('</body></html>'))
		.pipe(gulp.dest('dist/templates'));
});

gulp.task('hbs:default', function(cb) {
	return gulp.src('src/templates/*.hbs')
		.pipe(hb({
			partials: 'src/templates/globalPartials/**/*.hbs',
			data: 'src/templates/data/**/*.json'
		}))
		.on('error', cb)
		.pipe(rename({
			extname: '.html'
		}))
		.pipe(headerfooter.footer('</body></html>'))
		.pipe(gulp.dest('dist/templates'));
});

gulp.task('hbs:index', function(cb) {
	return gulp.src('src/*.hbs')
		.pipe(hb({
			partials: 'src/templates/globalPartials/**/*.hbs',
			data: 'src/templates/data/**/*.json'
		}))
		.on('error', cb)
		.pipe(rename({
			extname: '.html'
		}))
		.pipe(headerfooter.footer('</body></html>'))
		.pipe(gulp.dest('dist'));
});

gulp.task('sass:build', function() {
	return gulp.src('src/scss/main.scss')
		.pipe(sass())
		.pipe(postcss([
			autoprefixer({browsers: browsers}),
			cssnano()
		]))
		.pipe(concat('main.min.css'))
		.pipe(gulp.dest('dist/css'))
		.on('end', function() {
			console.log(gutil.colors.green('SASS BUILT'));
		});
});

gulp.task('sass:dev', function() {
	return gulp.src('src/scss/main.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.write())
		// .pipe(postcss([
		// 	autoprefixer({browsers: browsers})
		// ]))
		.pipe(concat('main.min.css'))
		.pipe(gulp.dest('dist/css'));
});

gulp.task('sass:transform', function() {
	return gulp.src('src/scss/**/*.scss')
		.pipe(stylefmt())
		.pipe(gulp.dest('src/scss'));
});

gulp.task('postcss', function() {
	return gulp.src('dist/css/main.min.css')
		.pipe(postcss([
			doiuse({
				browsers: browsers,
				onFeatureUsage: function(usageInfo) {
					console.log(usageInfo.message)
				}
			})
		]))
		.on('end', function() {
			console.log(gutil.colors.green('POSTCSS CHECK DONE'));
		});
});

gulp.task('images', function() {
	return gulp.src('src/images/**/*.+(png|jpg|gif|ico|json)')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/images'))
		.on('end', function() {
			console.log(gutil.colors.green('IMAGES BUILT'));
		});
});

gulp.task('images:svg', function() {
	return gulp.src('src/images/**/*.svg')
		.pipe(svgmin())
		.pipe(gulp.dest('dist/images'))
		.on('end', function() {
			console.log(gutil.colors.green('SVG BUILT'));
		});
});

gulp.task('js:build', function() {
	return gulp.src('src/js/*.js')
		.pipe(concat('script.js'))
		.pipe(minify({
			ext: {
				min: '.min.js'
			},
			ignoreFiles: ['-min.js', '.min.js']
		}))
		.pipe(gulp.dest('dist/js'))
		.on('end', function() {
			console.log(gutil.colors.green('JS BUILT'));
		});
});

gulp.task('js:dev', function() {
	return gulp.src('src/js/*.js')
		.pipe(concat('script.min.js'))
		.pipe(gulp.dest('dist/js'));
});

gulp.task('js:copy', function() {
	return gulp.src('src/js/lib/*.js')
		.pipe(gulp.dest('dist/js'));
});

gulp.task('fonts:copy', function() {
	return gulp.src('src/fonts/**/*')
		.pipe(gulp.dest('dist/fonts'));
});

gulp.task('files:copy', function() {
	return gulp.src('src/files/**/*')
		.pipe(gulp.dest('dist/files'));
});

gulp.task('root:copy', function() {
	return gulp.src('src/*.+(xml|txt)')
		.pipe(gulp.dest('dist'));
});

gulp.task('lint', (cb) => {
	gulpSequence('lint:sass', 'lint:scss', 'lint:html', 'lint:js', 'lint:json')(() => {
		console.log(gutil.colors.green('LINT DONE'));
		cb();
	});
});

gulp.task('lint:sass', function() {
	return gulp.src('src/scss/**/*.scss')
		.pipe(sassLint({
			configFile: '.lint-sass.yml'
		}))
		.pipe(sassLint.format())
		.pipe(sassLint.failOnError())
		.on('end', function() {
			gutil.log(gutil.colors.green('SASS LINT OK'));
		});
});

gulp.task('lint:scss', function() {
	return gulp.src('src/scss/**/*.scss')
		.pipe(gulpStylelint({
			reporters: [
				{formatter: 'string', console: true}
			]
		}))
		.on('end', function() {
			gutil.log(gutil.colors.green('SCSS LINT OK'));
		});
});

gulp.task('lint:html', function() {
	return gulp.src('dist/templates/**/*.html')
		.pipe(htmllint({}, (filepath, issues) => {
			if(issues.length > 0) {
				issues.forEach(function(issue) {
					gutil.log(gutil.colors.cyan('[gulp-htmllint] ') + gutil.colors.white(filepath + ' [' + issue.line + ',' + issue.column + ']: ') + gutil.colors.red('(' + issue.code + ') ' + issue.msg));
				});
				process.exitCode = 1;
			}
		}))
		.on('end', function() {
			gutil.log(gutil.colors.green('HTML LINT OK'));
		});
});

gulp.task('lint:js', function() {
	return gulp.src('src/js/*.js')
		.pipe(eslint('.lint-js.json'))
		.pipe(eslint.format())
		.pipe(eslint.result(result => {
			if(result.errorCount > 0 || result.warningCount > 0 || result.messages.length > 0) {
				gutil.log(gutil.colors.yellow('ESLint result: ' + result.filePath));
				gutil.log(gutil.colors.blue('# Messages: ' + result.messages.length));
				gutil.log(gutil.colors.red('# Warnings: ' + result.warningCount));
				gutil.log(gutil.colors.red('# Errors: ' + result.errorCount));
			}
		}))
		.on('end', function() {
			gutil.log(gutil.colors.green('JS LINT OK'));
		});
});

gulp.task('lint:json', function() {
	var testReporter = function(lint, file) {
		gutil.log(gutil.colors.red('Error: ' + file.path + ': ' + lint.error));
	};
	return gulp.src(['src/**/*.json'])
		.pipe(jsonlint())
		.pipe(jsonlint.report(testReporter))
		.on('end', function() {
			gutil.log(gutil.colors.green('JSON LINT OK'));
		});
});
